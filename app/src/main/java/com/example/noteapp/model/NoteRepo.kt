package com.example.noteapp.model

import com.example.noteapp.model.remote.NoteService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

object NoteRepo {
    private val noteService = object : NoteService {
        override suspend fun addNote(note: String): String {
            return note
        }
    }

    suspend fun addNote(note: String): String = withContext(Dispatchers.IO) {
        noteService.addNote(note)
    }
}