package com.example.noteapp.model.remote

interface NoteService {
    suspend fun addNote(note: String): String
}