package com.example.noteapp.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.noteapp.model.NoteRepo
import kotlinx.coroutines.launch

class NoteViewModel : ViewModel() {
    private val noteRepo by lazy { NoteRepo }

    private val _state = MutableLiveData<List<String>>()
    val state: LiveData<List<String>> get() = _state

    fun addNote(note: String) {
        viewModelScope.launch {
            val notes = _state.value
            if (notes != null) {
                // TODO: why isn't it adding another note to end of array
                val newNote = noteRepo.addNote(note)
                notes?.toMutableList()?.add(newNote)
                _state.value = notes!!
            } else {
                val newNote = noteRepo.addNote(note)
                _state.value = listOf(newNote)
            }
        }
    }
}