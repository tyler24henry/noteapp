package com.example.noteapp.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.noteapp.databinding.ItemLetterBinding

class LetterAdapter() : RecyclerView.Adapter<LetterAdapter.LetterViewHolder>() {
    private var letters = mutableListOf<String>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LetterViewHolder {
        val binding = ItemLetterBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return LetterViewHolder(binding)
    }

    override fun onBindViewHolder(holder: LetterViewHolder, position: Int) {
        val letter = letters[position]
        holder.loadLetter(letter)
    }

    override fun getItemCount(): Int = letters.size

    fun addLetters(letters: List<String>) {
        this.letters = letters.toMutableList()
        notifyDataSetChanged()
    }

    class LetterViewHolder(private val binding: ItemLetterBinding) : RecyclerView.ViewHolder(binding.root) {
        fun loadLetter(letter: String) {
            binding.tvLetter.text = letter
        }
    }
}