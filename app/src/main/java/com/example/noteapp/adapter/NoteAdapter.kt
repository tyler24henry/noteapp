package com.example.noteapp.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.noteapp.databinding.ItemNoteBinding
import com.example.noteapp.view.note.NoteFragmentDirections

class NoteAdapter : RecyclerView.Adapter<NoteAdapter.NoteViewHolder>() {

    private var notes = mutableListOf<String>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): NoteViewHolder {
        val binding = ItemNoteBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return NoteViewHolder(binding)
    }

    override fun onBindViewHolder(holder: NoteViewHolder, position: Int) {
        val note = notes[position]
        holder.loadNote(note)
        holder.itemView.setOnClickListener { view ->
            val directions = NoteFragmentDirections.actionNoteFragmentToLetterFragment(note)
            view.findNavController().navigate(directions)
        }
    }

    override fun getItemCount(): Int  = notes.size

    fun addNotes(notes: List<String>) {
        this.notes = notes.toMutableList()
        notifyDataSetChanged()
    }

    class NoteViewHolder(private val binding: ItemNoteBinding) : RecyclerView.ViewHolder(binding.root) {
        fun loadNote(note: String) {
            binding.tvNote.text = note
        }
    }
}






























