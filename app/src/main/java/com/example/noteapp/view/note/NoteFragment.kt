package com.example.noteapp.view.note

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.noteapp.adapter.NoteAdapter
import com.example.noteapp.databinding.FragmentNoteBinding
import com.example.noteapp.viewmodel.NoteViewModel

class NoteFragment : Fragment() {
    private var _binding: FragmentNoteBinding? = null
    private val binding get() = _binding!!
    private val noteViewModel by viewModels<NoteViewModel>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentNoteBinding.inflate(inflater, container, false).also { _binding = it }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.btnAddNote.setOnClickListener {
            noteViewModel.addNote(binding.etNote.text.toString())
        }
        binding.rvNotes.layoutManager = LinearLayoutManager(context)
        noteViewModel.state.observe(viewLifecycleOwner) {
            binding.rvNotes.adapter = NoteAdapter().apply {addNotes(noteViewModel.state.value ?: listOf("Note"))}
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}