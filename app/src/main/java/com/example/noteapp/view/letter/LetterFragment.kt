package com.example.noteapp.view.letter

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.noteapp.R
import com.example.noteapp.adapter.LetterAdapter
import com.example.noteapp.databinding.FragmentLetterBinding

class LetterFragment : Fragment(R.layout.fragment_letter) {
    private var _binding: FragmentLetterBinding? = null
    private val binding get() = _binding!!
    private val args by navArgs<LetterFragmentArgs>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentLetterBinding.inflate(inflater, container, false).also { _binding = it }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.rvLetters.layoutManager = LinearLayoutManager(context)

        val letters = args.letters.split("").slice(1 until args.letters.length + 1)
        binding.rvLetters.adapter = LetterAdapter().apply { addLetters(letters) }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}